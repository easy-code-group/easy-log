package com.our.base.examples.controller;

import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试的控制层
 *
 * @author poison
 * @DATE 2023/12/16
 **/
@Slf4j
@RequestMapping("easy/log")
@RestController
public class TestController {
    @GetMapping("/test")
    public String test(){
        UUID uuid = UUID.randomUUID();
        log.info("测试easy-log日志推送功能id:{}",uuid);
      return "此接口用于测试，调用接口后查看对应存储是否有"+uuid;

}
}
