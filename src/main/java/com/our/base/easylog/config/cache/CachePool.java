package com.our.base.easylog.config.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * 缓存池
 *
 * @author xiaowu
 * @date 2023/12/01
 **/
public class CachePool {
    private volatile static CachePool INSTANCE = null;

    /**
     * 缓存内容
     */
    private List<Object> list;
    /**
     * 大小
     */
    private final int maxSize;

    /**
     * 擦除位置
     */
    private int checkPoint = 0;

    /**
     * 写入位置
     */
    private int writePoint = 0;
    /**
     * 大小
     */
    private int size;

    public CachePool(int maxSize) {
        this.maxSize = maxSize;
        this.list = new ArrayList<>(maxSize);
    }

    /**
     * 查询指定条数的数据
     *
     * @param size
     * @return
     */

    public synchronized List<Object> getCache(int size) {
        return null;
    }

    /**
     * 获取缓存中所有数据
     *
     * @return
     */
    public synchronized List<Object> getAll() {
        return list;
    }

    /**
     * 写缓存
     *
     * @param data
     */
    public synchronized void writeCache(List<Object> data) {
        this.list.addAll(data);
    }

    public synchronized <T> void writeOneData(T data) {
        this.list.add(data);
    }

    public static CachePool getCachePool() {
        if (INSTANCE == null) {
            synchronized (CachePool.class) {
                if (INSTANCE == null) {
                    INSTANCE = new CachePool(100);
                }
            }
        }
        return INSTANCE;
    }

    public synchronized void cleanCache() {
        list = new ArrayList<>();
    }

    public int getSize() {
        return list.size();
    }
}
