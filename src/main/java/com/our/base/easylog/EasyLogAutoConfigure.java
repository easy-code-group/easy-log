package com.our.base.easylog;


import com.our.base.easylog.config.EasyLogProperties;
import com.our.base.easylog.es.EsPushLogImpl;
import com.our.base.easylog.config.es.EsProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;


/**
 * @author Alice
 */
@ComponentScan("com.our.base.easylog")
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Slf4j(topic = "easy-log")
@Configuration
@EnableConfigurationProperties({EsProperties.class, EasyLogProperties.class})
@ConditionalOnProperty(prefix = "easy-log", name = "enable", matchIfMissing = true)
public class EasyLogAutoConfigure {
    @Autowired
    private EasyLogProperties easyLogProperties;

    @Order(Ordered.HIGHEST_PRECEDENCE + 1)
    @Bean
    @ConditionalOnProperty(prefix = "easy-log", name = "storage", havingValue = "es")
    @ConditionalOnMissingBean(IPushLog.class)
    public IPushLog pushLog() {
        return new EsPushLogImpl(easyLogProperties.getEsProperties());
    }

}
