package com.our.base.easylog;

import java.util.ArrayList;
import java.util.List;

import com.our.base.easylog.constants.LogType;
import com.our.base.easylog.constants.PatternConstants;
import com.our.base.easylog.entity.EasyLogInfo;
import com.our.base.easylog.logback.LogbackInitializer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ConfigurableApplicationContext;

import lombok.Getter;

/**
 * EasyLog工厂类（用于注册框架以及对应日志组件）
 *
 * @author poison
 * @DATE 2023/12/10
 **/
public class EasyLogFactory {

    @Getter
    private static EasyLogInfo easyLogInfo;

    private EasyLogFactory() {
    }

    /**
     * 注册对应日志组件类型的信息
     *
     * @param logTypes                       日志类型
     * @param pattern                        格式化字符
     * @param configurableApplicationContext spring配置信息
     */
    public static void registerLogger(LogType[] logTypes, String pattern, ConfigurableApplicationContext configurableApplicationContext) {
        if (StringUtils.isBlank(pattern)) {
            pattern = PatternConstants.LOGBACK_DEFAULT_PATTERN;
        }
        String applicationName = configurableApplicationContext.getEnvironment().getProperty("spring.application.name");
        if (StringUtils.isBlank(applicationName)) {
            throw new RuntimeException("请设置applicationName");
        }
        easyLogInfo = new EasyLogInfo();
        easyLogInfo.setLogTypes(logTypes);
        easyLogInfo.setApplicationName(applicationName);
        List<String> className = new ArrayList<>();
        for (LogType logType : logTypes) {
            switch (logType) {
                case LOGBACK:
                    LogbackInitializer logbackInitializer = new LogbackInitializer(pattern);
                    className.add(logbackInitializer.getInitializerClassName());
                    break;
                default:


            }
            easyLogInfo.setRegisterClassName(className.toArray(className.toArray(new String[]{})));


        }

    }

}
