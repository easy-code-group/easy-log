  **本项目目的** ： 
  实现轻量化日志推送管理框架：引入一个jar，配置一下yml ，然后项目中的日志自动推送到对应存储中去，并且兼容主流日志组件与主流存储方式。
提供一个日志搜索平台，可以根据项目名称，方法，错误级别，等自定义格式去搜索。提供日志报警推送功能。支持规则配置与主流推送渠道。（抄毒药的就是我想实现的效果）
项目目前处于公开状态可以随时加入,项目加入地址：[项目地址](https://gitee.com/itjunWu/easy-log/invite_link?invite=2199b0f919800d858c6a6d20b6676edd3d4d4fd5df7b2399b7993663152babc5eef6529b002e07b603eac9232b9ccfc5)
你可以选择加入项目或者fork代码下来自己提交合并请求，项目初期阶段，无任何要求。可以提新的功能点和需求，也可以直接写代码，有代码看不懂或者有问题可以在群里问
采集日志框架基本流程：
1. LoggingApplicationListener进行日志初始化。
2. EasyLogInitializer根据已有的日志组件类型，进行不同的日志拦截器注册。这一步在Spring输出banner之前进行，且在初始化ApplicationContextInitializer前完成。注意此时Spring还未装载任何Bean，所以所有操作需要手动创建。
3. 初始化ApplicationContextInitializer。
4. AOP注入拦截器，拦截所有的接口方法。每次请求时，将请求方法放到MDC里，请求完成后清除MDC，实现链路跟踪。
5. Spring启动成功，回调StartupApplicationListener。启动两个线程，一个线程监控缓存池是否已满，另一个监控时间。满了就直接调用存储接口推送，推送逻辑由具体存储实现负责。
6. 如果Spring启动失败，回调FailedApplicationListener。尝试对应存储实现是否可用，不可用则把日志暂存到文件，后续平台会有jar扫描对应目录进行读取推送。
中间任何一个流程大家都可以选择性实现，要实现那个部分就在下面的文档中写上功能点和实现人就好了。
:)
[腾讯文档在线地址](https://docs.qq.com/doc/DS29VRkJXT3JWVkxO)
所有可接受任务都在里面，如果有要接受的群里叫一下分配给你