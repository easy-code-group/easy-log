package com.our.base.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试例子启动类
 * 作者：张翠山
 * 日期: 2023/12/16
 */
@SpringBootApplication
public class EasyLogExamplesApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyLogExamplesApplication.class, args);
    }
}
