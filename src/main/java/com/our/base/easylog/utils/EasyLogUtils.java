package com.our.base.easylog.utils;

import java.util.ArrayList;
import java.util.List;

import com.our.base.easylog.constants.Constants;
import com.our.base.easylog.constants.LogType;

/**
 * 框架工具类
 *
 * @author poison
 * @DATE 2023/12/10
 **/
public class EasyLogUtils {

    /**
     * 判断对应class是否存在，存在返回true否则返回false
     * @param className 要检查的class
     * @return 检查结果
     */
    private static boolean isClassPresent(String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException ex) {
            return false;
        }
    }

    /**
     * 检查依赖的日志组件
     * @return 返回对应支持的日志组件枚举，不支持或者没找到返回null
     */
    public static LogType[] checkLoggingLibrary() {
        List<LogType> logTypes=new ArrayList<>();
        if (isClassPresent(Constants.LOG4J2_CLASS_NAME)) {
           logTypes.add(LogType.LOG4J2);
        }
         if (isClassPresent(Constants.LOG4J_CLASS_NAME)) {
            logTypes.add(LogType.LOG4J);
        }
         if (isClassPresent(Constants.LOGBACK_CLASS_NAME)) {
            logTypes.add(LogType.LOGBACK);
        }

        return logTypes.toArray(new LogType[]{});
    }
}
