package com.our.base.easylog;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import com.our.base.easylog.config.EasyLogProperties;
import com.our.base.easylog.constants.LogType;
import com.our.base.easylog.utils.Banner;
import com.our.base.easylog.utils.EasyLogUtils;
import com.our.base.easylog.utils.SpringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 框架初始化类
 *
 * @author poison
 * @DATE 2023/12/10
 **/

@Slf4j(topic = "easy-log")
public class EasyLogInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        EasyLogProperties easyLogConfig = SpringUtils.getEasyLogConfig(configurableApplicationContext);
        boolean enable = easyLogConfig.isEnable();

        if (!enable) {
            return;
        }
        String patternStr = "";
        Object pattern = easyLogConfig.getPattern();
        if (Objects.nonNull(pattern)) {
            patternStr = pattern.toString();
        }

        Banner.print();

        LogType[] logTypes = EasyLogUtils.checkLoggingLibrary();
        if (logTypes.length == 0) {
            System.err.println("easy-log没有找到对应支持的日志组件，请添加对应依赖" + Arrays.toString(LogType.values()));
            return;
        }
        //此时的日志不会记录
        if (logTypes.length > 1) {
            log.info("检测到多个日志组件{}", Arrays.toString(logTypes));
        }
        if (logTypes.length == 1) {
            log.info("日志类型:[{}]", logTypes[0]);
        }
        EasyLogFactory.registerLogger(logTypes, patternStr,configurableApplicationContext);
    }


}
