package com.our.base.easylog;

/**
 * 日志推送接口
 *
 * @author Alice
 * @date 2023/11/29
 **/
public interface IPushLog {
    /**
     * 推送日志
     *
     */
    void pushLog();
}
