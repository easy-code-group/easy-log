package com.our.base.easylog;

/**
 * 初始化接口
 *
 * @author poison
 * @DATE 2023/12/10
 **/
public  abstract class Initializer {
    protected String initializerClassName;

    /**
     * 返回对应的注册的日志拦截器class全称
     * @return 返回对应的注册的日志拦截器class全称
     */
    protected abstract String  getInitializerClassName();

    public void setInitializerClassName(String initializerClassName) {
        initializerClassName = initializerClassName;
    }
}
