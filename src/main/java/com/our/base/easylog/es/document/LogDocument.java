package com.our.base.easylog.es.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDocument {
    /**
     * 日志正文
     */
    private String log;


    /**
     * 日志存入时间
     */
    private Date logTime;
    /**
     * mdc(条件)
     */
    private Map<String,String> mdc;

}

