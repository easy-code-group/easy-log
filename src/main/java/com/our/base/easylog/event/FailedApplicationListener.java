package com.our.base.easylog.event;

import com.our.base.easylog.utils.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author wuyang
 * @Description springboot 启动失败监听器
 * @date 2023/12/8
 **/
@Component
@Slf4j
public class FailedApplicationListener implements ApplicationListener<ApplicationFailedEvent> {
    @Override
    public void onApplicationEvent(ApplicationFailedEvent applicationFailedEvent) {
        SpringUtils.failedSpring();
        log.info("日志推送服务启动失败。");
        // TODO 调用平台接口处理推送失败日志
    }
}
