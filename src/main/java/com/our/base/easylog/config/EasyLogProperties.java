package com.our.base.easylog.config;

import com.our.base.easylog.config.es.EsProperties;
import com.our.base.easylog.constants.PatternConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 主配置文件
 *
 * @author Alice
 * @date 2023/11/29 123
 **/
@Data
@ConfigurationProperties(prefix = "easy-log")
public class EasyLogProperties {
    /**
     * 是否启用
     */
    private boolean enable = false;
    /**
     * 最大缓存数
     */
    private int maxCache=10;
    /**
     * 最大时间
     */
    private long maxWaitTime = 60;
    /**
     * 存储方式
     */
    private String storage="es";
    /**
     * 格式化
     */
    private String pattern = PatternConstants.LOGBACK_DEFAULT_PATTERN;

    /**
     * es配置
     */
    @NestedConfigurationProperty
    private EsProperties esProperties = new EsProperties();;
}
