package com.our.base.easylog.config.es;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * auth: xiaowu
 * es 配置信息
 */
@Data
@ConfigurationProperties(prefix = "easy-log.es")
public class EsProperties {
    /**
     * 集群地址
     */
    private String host;

    /**
     * 用户名
     */
    private String user;

    /**
     * 密码
     */
    private String password;

    /**
     * schema 模式
     */
    private String schema = "http";

    /**
     * es默认schema
     */
    String DEFAULT_SCHEMA = "http";

    /**
     * socketTimeout timeUnit:millis  通讯超时时间 单位毫秒 默认10分钟 以免小白踩坑平滑模式下数据量过大迁移超时失败
     */
    private Integer socketTimeout = 600000;
    /***
     * 保持心跳时间 timeUnit:millis  单位毫秒 默认30秒 防止小白白出现服务首次调用不成时不知所措
     */
    private Integer keepAliveMillis = 30000;
    /**
     * maxConnectTotal 最大连接数
     */
    private Integer maxConnTotal = 20;

    /**
     * maxConnectPerRoute 最大连接路由数
     */
    private Integer maxConnPerRoute;

    /**
     * connectTimeout timeUnit:millis 连接超时时间 单位毫秒
     */
    private Integer connectTimeout;

    /**
     * connectionRequestTimeout timeUnit:millis 连接请求超时时间 单位毫秒
     */
    private Integer connectionRequestTimeout;

    /**
     * 索引配置模板
     */
    private String template;

    /**
     * 管道名
     */
    private String[] pipeline;

    /**
     * 管道配置文件地址
     */
    private String pipelineUrl;
}
