package com.our.base.easylog.constants;

/**
 * @author wuyang
 * @Description 日志格式常量
 * @date 2023/12/7
 **/
public class PatternConstants {
    /**
     * logback默认格式
     */
    public  static final String LOGBACK_DEFAULT_PATTERN="%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n";

}
