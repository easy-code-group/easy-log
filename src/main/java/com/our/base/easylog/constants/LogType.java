package com.our.base.easylog.constants;

/**
 * 日志组件类型
 *
 * @author poison
 * @DATE 2023/12/10
 **/
public enum LogType {
    /**
     * logback
     */
    LOGBACK,
    /**
     * log4j
     */
    LOG4J,
    /**
     * log4j2
     */
    LOG4J2
}
