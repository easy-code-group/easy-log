package com.our.base.easylog.utils;

import java.util.List;
import java.util.concurrent.*;

/**
 * 线程池工具类
 *
 * @author chongchong
 */
public class ThreadPoolUtil {
    /**
     * 工具类，构造方法私有化
     */
    private ThreadPoolUtil() {
        super();
    }

    /**
     * 线程池核心线程数
     */
    private final static Integer COREPOOLSIZE = 5;
    /**
     * 最大线程数
     */
    private final static Integer MAXIMUMPOOLSIZE = 10;
    /**
     * 空闲线程存活时间
     */
    private final static Integer KEEPALIVETIME = 180;
    /**
     * 线程池核心线程数
     */
    private static final BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(10);
    /**
     * 线程池对象
     */
    private static final ThreadPoolExecutor threadPool = new ThreadPoolExecutor(COREPOOLSIZE, MAXIMUMPOOLSIZE,
            KEEPALIVETIME, TimeUnit.SECONDS, queue, new ThreadPoolExecutor.AbortPolicy());

    /**
     * 向线程池提交一个任务,返回线程结果
     *
     * @param r
     * @return
     */
    public static Future<?> submit(Callable<?> r) {
        return threadPool.submit(r);
    }

    /**
     * 向线程池提交一个任务,返回线程结果
     *
     * @param rs
     * @return
     */
    public static List<Future<?>> submits(List rs) {
        try {
            List list = threadPool.invokeAll(rs);
            return list;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 向线程池提交一个任务，不关心处理结果
     *
     * @param r
     */
    public static void execute(Runnable r) {
        threadPool.execute(r);
    }

    /**
     * 获取当前线程池线程数量
     */
    public static int getSize() {
        return threadPool.getPoolSize();
    }

    /**
     * 获取当前活动的线程数量
     */
    public static int getActiveCount() {
        return threadPool.getActiveCount();
    }
}
