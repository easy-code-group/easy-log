package com.our.base.easylog.logback;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.our.base.easylog.Initializer;
import com.our.base.easylog.logback.appender.PushLogAppender;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

/**
 * @author poison
 * @Description logback 初始化实现 （在spring boot 初始化之前）
 * @date 2023/12/6
 **/
@Slf4j(topic = "easy-log")
public class LogbackInitializer extends Initializer {


    public LogbackInitializer(String pattern) {

        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        JoranConfigurator configurator = new JoranConfigurator();

        String springLogbackFile = "logback-spring.xml";
        String logbackFilePath = "logback.xml";
        configurator.setContext(loggerContext);
        URL springLogbackUrl = getClass().getClassLoader().getResource(springLogbackFile);
        URL logbackUrl = getClass().getClassLoader().getResource(logbackFilePath);
        try {
            if (springLogbackUrl != null) {
                configurator.doConfigure(springLogbackUrl);
            } else if (logbackUrl != null) {
                configurator.doConfigure(logbackUrl);
            } else {
                log.warn("没有找到logback配置文件");
            }
        } catch (JoranException exception) {
            log.warn("没有正确加载logback配置文件:" + exception.getMessage());
        }

        PatternLayout layout = new PatternLayout();
        layout.setPattern(pattern);
        layout.setContext(loggerContext);
        layout.start();
        PushLogAppender pushLogAppender = new PushLogAppender();
        pushLogAppender.setContext(loggerContext);
        pushLogAppender.setName("logPush");
        pushLogAppender.setLayout(layout);
        pushLogAppender.start();

        ch.qos.logback.classic.Logger logger = loggerContext.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.setLevel(Level.INFO);
        logger.addAppender(pushLogAppender);
        setInitializerClassName(pushLogAppender.getClass().getName());
        log.info("Easy-logLogBack日志拦截器(Appender)已加入成功。");
    }


    @Override
    public String getInitializerClassName() {
        return initializerClassName;
    }
}
