package com.our.base.easylog.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

/**
 * @author wuyang
 * @Description banner 工具类
 * @date 2023/12/8
 **/

public class Banner {
    /**
     * 重置文本变量
     */
    private final static String  RESET = "\033[0m";
       public static void   print(){
            ResourceLoader resourceLoader=new DefaultResourceLoader();
            Resource resource = resourceLoader.getResource("classpath:Easy-Log.txt");


            try {
                InputStream inputStream = resource.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);

                }
                reader.close();
                System.out.println("\033[0;32mEasy-log:                    "+RESET+" 0.1.1-SNAPSHOT");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }



}
