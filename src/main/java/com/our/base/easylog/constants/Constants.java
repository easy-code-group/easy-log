package com.our.base.easylog.constants;

/**
 * 常量表
 *
 * @author poison
 * @DATE 2023/12/10
 **/
public class Constants {

    /**
     * logback对应的class名称
     */
    public final static String LOGBACK_CLASS_NAME = "ch.qos.logback.classic.LoggerContext";
    /**
     * log4j对应的class名称
     */
    public final static String LOG4J_CLASS_NAME = "org.apache.log4j.Logger";
    /**
     * log4j2对应的class名称
     */
    public final static String LOG4J2_CLASS_NAME = "org.apache.logging.log4j.Logger";

}
