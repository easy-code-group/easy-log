package com.our.base.easylog.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

import org.slf4j.MDC;

/**
 * 过滤器
 * <p>
 * name:李瑞兵
 *
 * @author admin
 * @date 2023/12/11
 */
@Slf4j
@WebFilter
public class ReqFilter implements Filter {

    public ReqFilter() {
        System.out.println("init reqFilter");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        try {
            HttpServletRequest req = (HttpServletRequest) request;
            String requestURI = req.getRequestURI();
            MDC.put("requestUrl", requestURI);
            chain.doFilter(req, response);
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            MDC.clear();
        }
    }

    @Override
    public void destroy() {

    }
}