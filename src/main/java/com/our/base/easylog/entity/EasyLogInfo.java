package com.our.base.easylog.entity;

import java.util.List;

import com.our.base.easylog.constants.LogType;

import lombok.Data;

/**
 * 框架信息
 *
 * @author poison
 * @DATE 2023/12/10
 **/
@Data
public class EasyLogInfo {

    /**
     * 日志组件类型
     */
    private LogType[] logTypes;
    /**
     * 注册的拦截器类名
     */
    private String[] registerClassName;
    /**
     * 应用名称
      */
    private String applicationName;




}
